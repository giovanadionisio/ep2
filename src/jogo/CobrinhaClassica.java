package jogo;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class CobrinhaClassica extends Cobrinha {	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CobrinhaClassica frame = new CobrinhaClassica();
					frame.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public CobrinhaClassica() {
		iniciar_classica();
	}
	
	public void iniciar_classica() {
		add(new InterfaceClassica());
		setResizable(false);
		pack();
		setTitle("Cobrinha Clássica");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
