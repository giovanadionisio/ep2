package jogo;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class CobrinhaStar extends Cobrinha {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CobrinhaClassica frame = new CobrinhaClassica();
					frame.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public CobrinhaStar() {
		iniciarStar();
	}
	
	public void iniciarStar() {
		add(new InterfaceStar());
		setResizable(false);
		pack();
		setTitle("Cobrinha Star");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
