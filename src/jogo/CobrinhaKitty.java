package jogo;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class CobrinhaKitty extends Cobrinha {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CobrinhaKitty frame = new CobrinhaKitty();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public CobrinhaKitty() {
		iniciarKitty();
	}

	public void iniciarKitty() {
		add(new InterfaceKitty());
		setResizable(false);
		pack();
		setTitle("Cobrinha Kitty");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
