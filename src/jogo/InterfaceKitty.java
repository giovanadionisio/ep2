package jogo;

import java.lang.Thread;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.util.TimerTask;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.Random;
import javax.swing.SwingConstants;

public class InterfaceKitty extends JPanel implements ActionListener {
	private final int ALTURA = 390;
	private final int LARGURA = 400;
	private final int TAMANHO_PARTES = 10;
	private final int PARTES_TOTAIS = (ALTURA * LARGURA)/(TAMANHO_PARTES * TAMANHO_PARTES);
	private final int AUX_ALEATORIO = 38;
	private final int TIPOS_POINT = 4;
	private final int DELAY = 140;
	private final int N_ARBUSTOS = 6;
	
	private final int x[] = new int[PARTES_TOTAIS];
	private final int y[] = new int [PARTES_TOTAIS];
	
	private final int arbustos_x[] = new int[N_ARBUSTOS];
	private final int arbustos_y[] = new int[N_ARBUSTOS];
	
	private int comprimento;
	private int point_x;
	private int point_y;
	private int score;

	private Image inicial;
	private Image corpo;
	private Image point;
	private Image dpoint;
	private Image bomba;
	private Image decrease;
	private Image arbusto;
	private Image ponto_da_vez;
	private Graphics g;
	
	private boolean comeu = false;
	private boolean esquerda = false;
	private boolean direita = true;
	private boolean cima = false;
	private boolean baixo = false;
	private boolean jogando = true;
	
	private Timer timer;
	private JTextField txtScore;
	private JTextField txtContagem;
	
	public InterfaceKitty() {
		addKeyListener(new TAdapter());
		setPreferredSize(new Dimension(400, 410));
		setBackground(new Color(254, 240, 227));
		setFocusable(true);
		setLayout(new BorderLayout(0, 0));
		
		txtScore = new JTextField();
		txtScore.setPreferredSize(new Dimension(10, 19));
		txtScore.setBorder(null);
		txtScore.setHorizontalAlignment(SwingConstants.CENTER);
		txtScore.setFont(new Font("Nimbus Mono L", Font.BOLD, 14));
		txtScore.setBackground(new Color(240, 173, 128));
		txtScore.setText("Score: " + score);
		add(txtScore, BorderLayout.SOUTH);
		txtScore.setColumns(10);
		
		Introducao();
	}
	
	private void Introducao() {
		txtContagem = new JTextField();
		txtContagem.setPreferredSize(new Dimension(50, 50));
		txtContagem.setBorder(null);
		txtContagem.setHorizontalAlignment(SwingConstants.CENTER);
		txtContagem.setFont(new Font("Nimbus Mono L", Font.BOLD, 20));
		txtContagem.setBackground(new Color(254, 240, 227));
		add(txtContagem, BorderLayout.CENTER);
		
		new Thread() {
			public void run() {
				int i;
				for(i = 3; i>=0; i--){	
					try{	
						Thread.sleep(1000);
					} catch (InterruptedException e){	
						System.out.println("ERRO");
					}
					
					if(i == 0) {
						txtContagem.setText("****** GO! ******");
						remove(txtContagem);
						CarregarImagens();
						IniciarJogo ();	
					} else {
						txtContagem.setText("******" + i + "******");
						add(txtContagem, BorderLayout.CENTER);
					}
				}
			}
		}.start();	
	}
	
	private void CarregarImagens() {
		ImageIcon iiinicial = new ImageIcon("src/imagens/inicial_k.png");
		inicial = iiinicial.getImage();
		ImageIcon iicorpo = new ImageIcon("src/imagens/corpo_k.png");
		corpo = iicorpo.getImage();
		ImageIcon iipoint = new ImageIcon("src/imagens/point.png");
		point = iipoint.getImage();
		ImageIcon iidpoint = new ImageIcon("src/imagens/dpoint.png");
		dpoint = iidpoint.getImage();
		ImageIcon iidbomba = new ImageIcon("src/imagens/bomb.png");
		bomba = iidbomba.getImage();
		ImageIcon iidecrease = new ImageIcon("src/imagens/decrease.png");
		decrease = iidecrease.getImage();
		ImageIcon iiarbusto = new ImageIcon("src/imagens/arbusto.png");
		arbusto = iiarbusto.getImage();
		
		timer = new Timer(DELAY, this);
		timer.start();
	}
	
	private void IniciarJogo() {
		comprimento = 2;
		for (int z = 0; z < comprimento; z++) {
			x[z] = 50 - z * 10;
			y[z] = 50;
		}
		LocalizarArbusto();
		LocalizarPoint();
		ChamaLocalizar();
		return;
	}
	
	private void LocalizarArbusto() {
		Random n_aleatorio = new Random();
		
		for (int i = 0; i < N_ARBUSTOS; i++) {
			arbustos_x[i] = TAMANHO_PARTES * n_aleatorio.nextInt(AUX_ALEATORIO);
			arbustos_y[i] = TAMANHO_PARTES * n_aleatorio.nextInt(AUX_ALEATORIO);
		}
	}
	
	private void LocalizarPoint() {
		Random escolhe_tipo = new Random();
		boolean conflito = false;
		int x, y;
		
		if(escolhe_tipo.nextInt(TIPOS_POINT) == 0) {
			ponto_da_vez = dpoint;
		} else if(escolhe_tipo.nextInt(TIPOS_POINT) == 1) {
			ponto_da_vez = point;
		} else if (escolhe_tipo.nextInt(TIPOS_POINT) == 2) {
			ponto_da_vez = bomba;
		} else if (escolhe_tipo.nextInt(TIPOS_POINT) == 3) {
			ponto_da_vez = decrease;
		}
		
		do {
			Random n_aleatorio = new Random();
			x = TAMANHO_PARTES * n_aleatorio.nextInt(AUX_ALEATORIO);
			y = TAMANHO_PARTES * n_aleatorio.nextInt(AUX_ALEATORIO);	
			
			for (int i = 0; i < N_ARBUSTOS; i++) {
				if (x == arbustos_x[i] && y == arbustos_y[i]) {
					conflito = true;
					break;
				}
			}
		} while (conflito);
		point_x = x;
		point_y = y;
		conflito = false;			
	}
	
	private void ChamaLocalizar() { 
		
		new Thread() {
			 public void run() {
				 while(true) {
					 if(!jogando) {
						 return;
					 } else if (comeu) {
						 comeu = false;
						 return;
					 } else {
						 try {
							 Thread.sleep(1000);
							 Thread.sleep(1000);
							 Thread.sleep(1000);
							 Thread.sleep(1000);
							 Thread.sleep(1000);
							 Thread.sleep(1000);
							 Thread.sleep(1000);
							 Thread.sleep(1000);
							 Thread.sleep(1000);
							 Thread.sleep(1000);
						 } catch (InterruptedException e) {
							e.printStackTrace();
						 }
						 LocalizarPoint();
					 } 
				 }
			 }
		 }.start();
	}
	
	private void ContaPonto() {
		new Thread() {
			public void run() {
				score++;
			}
		}.start();
	}
	
	private void AtualizaVisor() {
		new Thread() {
			public void run() {
				txtScore.setText("Score: " + score);
			}
		}.start();
	}
	
	private void ComePoint() {
		if ((x[0] == point_x) && (y[0] == point_y)) {
			if(ponto_da_vez == bomba) {
				jogando = false;
				return;
			} else if(ponto_da_vez == decrease) {
				comprimento = 2;
			} else {
				comprimento++;
				ContaPonto();
				if(ponto_da_vez == dpoint) {
					ContaPonto();
				}
			}
			
			AtualizaVisor();
			comeu = true;
			LocalizarPoint();
			ChamaLocalizar();
			return;
		}
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		doDrawing(g);
	}
	
	private void doDrawing(Graphics g) {
		if(jogando) {
			for(int i = 0; i<N_ARBUSTOS; i++) {
				g.drawImage(arbusto, arbustos_x[i], arbustos_y[i], this);
			}
			g.drawImage(ponto_da_vez, point_x, point_y, this);
			for (int z = 0; z < comprimento; z++) {
				if (z == 0) {
					g.drawImage(inicial, x[z], y[z], this);
	            } else {
	                g.drawImage(corpo, x[z], y[z], this);
	            }
			}
	        Toolkit.getDefaultToolkit().sync();  
		} else {
			GameOver(g);
			timer.stop();
		}
    }
	
	private void move() {

        for (int z = comprimento; z > 0; z--) {
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }

        if (esquerda) {
            x[0] -= TAMANHO_PARTES;
        }

        if (direita) {
            x[0] += TAMANHO_PARTES;
        }

        if (cima) {
            y[0] -= TAMANHO_PARTES;
        }

        if (baixo) {
            y[0] += TAMANHO_PARTES;
        }
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		if(jogando) {
			verificaColisao();
			ComePoint();
			move();
		}
		repaint();
	}

	private class TAdapter extends KeyAdapter {
			public void keyPressed(KeyEvent e) {

	            int key = e.getKeyCode();

	            if ((key == KeyEvent.VK_LEFT) && (!direita)) {
	                esquerda = true;
	                cima = false;
	                baixo = false;
	            }

	            if ((key == KeyEvent.VK_RIGHT) && (!esquerda)) {
	            	direita = true;
	            	cima = false;
	            	baixo = false;
	            }

	            if ((key == KeyEvent.VK_UP) && (!baixo)) {
	            	cima = true;
	                direita = false;
	                esquerda = false;
	            }

	            if ((key == KeyEvent.VK_DOWN) && (!cima)) {
	            	baixo = true;
	                direita = false;
	                esquerda = false;
	            }
	        }
		};
		
		private void verificaColisao() {
			for(int z = comprimento; z > 0; z--) {
				if(z > 4 && (x[0] == x[z]) && (y[0] == y[z])){
					jogando = false;
				}
			}
			
			if(x[0] < 0){
				jogando = false;
			} else if(x[0] == LARGURA) {
				jogando = false;
			} else if(y[0] < 0) {
				jogando = false;
			} else if(y[0] == ALTURA) {
				jogando = false;
			}
			
		}
		
		private void GameOver(Graphics g) {
			String mensagem = "*****Game Over*****";
			String pontuação = "Score: " + score;
			Font fonte = new Font("Nimbus Mono L", Font.BOLD, 20);
			FontMetrics metrics = getFontMetrics(fonte);
			
			g.setColor(Color.black);
	        g.setFont(fonte);
	        g.drawString(mensagem, (ALTURA - metrics.stringWidth(mensagem)) / 2, LARGURA / 3);
	        g.drawString(pontuação, (ALTURA - metrics.stringWidth(pontuação)) / 2, LARGURA / 2);
		}
}
