package jogo;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;

public class MenuInicial extends JFrame{

	private JPanel contentPane;
	private CobrinhaClassica cobrinha_classica;
	private CobrinhaKitty cobrinha_kitty;
	private CobrinhaStar cobrinha_star;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuInicial frame = new MenuInicial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MenuInicial() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setFont(new Font("Nimbus Sans L", Font.PLAIN, 16));
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		contentPane.setBackground(new Color(255, 191, 192));
		contentPane.setMaximumSize(new Dimension(400, 410));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setTitle("Menu Inicial");
		contentPane.setLayout(null);
		
		JButton btnClssica = new JButton("Clássica");
		btnClssica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnClssica.setFont(new Font("Nimbus Mono L", Font.BOLD, 13));
		btnClssica.setBounds(26, 27, 99, 25);
		btnClssica.setBackground(new Color(213, 230, 150));
		contentPane.add(btnClssica);
		
		JLabel lblNewLabel = new JLabel("Bomba: Cuidado! Levam à morte da cobrinha.");
		lblNewLabel.setFont(new Font("Nimbus Mono L", Font.BOLD, 14));
		lblNewLabel.setIcon(new ImageIcon(MenuInicial.class.getResource("/imagens/bomba_red.png")));
		lblNewLabel.setMaximumSize(new Dimension(30, 30));
		lblNewLabel.setBounds(39, 211, 437, 25);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Não encoste dos arbustos!");
		lblNewLabel_1.setFont(new Font("Nimbus Mono L", Font.BOLD, 14));
		lblNewLabel_1.setIcon(new ImageIcon(MenuInicial.class.getResource("/imagens/arbusto_red.png")));
		lblNewLabel_1.setBounds(87, 248, 290, 15);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon(MenuInicial.class.getResource("/imagens/arbusto_red.png")));
		lblNewLabel_2.setBounds(327, 248, 66, 15);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Simples: aumenta em um o tamanho e o score");
		lblNewLabel_3.setFont(new Font("Nimbus Mono L", Font.BOLD, 14));
		lblNewLabel_3.setIcon(new ImageIcon(MenuInicial.class.getResource("/imagens/point_red.png")));
		lblNewLabel_3.setBounds(26, 108, 402, 41);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Duplo: adiciona dois pontos ao score");
		lblNewLabel_4.setFont(new Font("Nimbus Mono L", Font.BOLD, 14));
		lblNewLabel_4.setIcon(new ImageIcon(MenuInicial.class.getResource("/imagens/dpoint_red.png")));
		lblNewLabel_4.setBounds(49, 145, 357, 30);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Decrementa: volta o comprimento para o inicial");
		lblNewLabel_5.setIcon(new ImageIcon(MenuInicial.class.getResource("/imagens/decrease_red.png")));
		lblNewLabel_5.setFont(new Font("Nimbus Mono L", Font.BOLD, 14));
		lblNewLabel_5.setBounds(12, 184, 426, 15);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Use as setas do teclado para jogar");
		lblNewLabel_6.setFont(new Font("Nimbus Mono L", Font.BOLD, 14));
		lblNewLabel_6.setIcon(new ImageIcon(MenuInicial.class.getResource("/imagens/setas_red.png")));
		lblNewLabel_6.setBounds(63, 68, 387, 41);
		contentPane.add(lblNewLabel_6);
		
		JButton btnKitty = new JButton("Kitty");
		btnKitty.setMinimumSize(new Dimension(88, 25));
		btnKitty.setMaximumSize(new Dimension(88, 25));
		btnKitty.setBackground(new Color(255, 127, 80));
		btnKitty.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnKitty.setFont(new Font("Nimbus Mono L", Font.BOLD, 14));
		btnKitty.setBounds(157, 27, 114, 25);
		contentPane.add(btnKitty);
		
		JButton btnStar = new JButton("Star");
		btnStar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnStar.setMinimumSize(new Dimension(88, 25));
		btnStar.setMaximumSize(new Dimension(88, 25));
		btnStar.setBackground(new Color(255, 255, 102));
		btnStar.setFont(new Font("Nimbus Mono L", Font.BOLD, 13));
		btnStar.setBounds(301, 27, 114, 25);
		contentPane.add(btnStar);
		
		btnClssica.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cobrinha_classica = new CobrinhaClassica();
				cobrinha_classica.setVisible(true);
				dispose();
			}
		});
		
		btnKitty.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cobrinha_kitty = new CobrinhaKitty();
				cobrinha_kitty.setVisible(true);
				dispose();
			}
		});
		
		btnStar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cobrinha_star = new CobrinhaStar();
				cobrinha_star.setVisible(true);
				dispose();
			}
		});
		
		
	}
}
